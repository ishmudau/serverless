'use strict';

require('dotenv').config({ path: './variables.env' });

const connectToDatabase = require('./db');
const User = require('./models/User');
var _ = require('underscore');

//Create and Save a new user
exports.create = (event, context, callback) => {
   // Validate request
   // callbackWaitsForEmptyEventLoop – Set to false to send the response right away when the callback runs,
   // instead of waiting for the Node.js event loop to be empty.
   // If this is false, any outstanding events continue to run during the next invocation.
   context.callbackWaitsForEmptyEventLoop = false;

   connectToDatabase()
     .then(() => {
       User.create(JSON.parse(event.body))
       .then(data => callback(null, {
         statusCode: 200,
         body: JSON.stringify({result: "success"})
       }))
       .catch(err => callback(null, {
         statusCode: err.statusCode || 500,
         body: JSON.stringify({ result: "FailToaddUser" })
       }))

     });
};

// Retrieve and return all users from the database.
exports.findAll = (event, context, callback) => {
  context.callbackWaitsForEmptyEventLoop = false;

  connectToDatabase()
    .then(() => {
      User.find()
        .then(users => callback(null, {
          statusCode: 200,
          headers: { 'Content-Type': 'application/json' },
          body: JSON.stringify(users)
        }))
        .catch (err => callback(null, {
          statusCode: err.statusCode || 500,
          headers: { 'Content-Type': 'text/plain' },
          body: JSON.stringify({result : "NoUsers."})
        })) ;
  })
  .catch(err => callback(null, {
    statusCode : err.statusCode || 500,
    headers: {'Content-Type': 'text/plain' },
    body : JSON.stringify({result : "Could Not Fetch Users"})
  }))
};

// Find a single user with a id
exports.findOnes = (event, context, callback) => {
  context.callbackWaitsForEmptyEventLoop = false;
  connectToDatabase()
    .then(() => {
      User.findOne({userId: event.pathParameters.id})
      .then(user => callback(null, {
        statusCode: 200,
        body: JSON.stringify(user)
      }))
      .catch (err => callback(null, {
          statusCode: err.statusCode || 500,
          headers: { 'Content-Type': 'application/json' },
          body: JSON.stringify({result : "UserNotFound"})
      }));
  });
};

// Find a single user with a id
exports.findByEmail = (event, context, callback) => {
  context.callbackWaitsForEmptyEventLoop = false;
  connectToDatabase()
    .then(() => {
      User.find({email: event.pathParameters.email})
      .then(user => callback(null, {
        statusCode: 200,
        body: JSON.stringify(user)
      }))
      .catch(err => callback(null, {
        statusCode: err.statusCode || 500,
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({result : "UserNotFound"})
      }))
  });
};

// Update a user identified by the id in the request
exports.update = (event, context, callback) => {
  context.callbackWaitsForEmptyEventLoop = false;
  connectToDatabase()
    .then(() => {
    if(!event.body == null) {
      callback( null, {body: JSON.stringify('Body is Empty' )});
    }
    // Find user and update it with the request body
    User.findOneAndUpdate({userId: event.pathParameters.id}, JSON.parse(event.body), {new: true})
    .then(user => callback(null, {
      statusCode: 200,
      body: JSON.stringify(user)
    })).catch(err => callback(null, {
      statusCode: 404,
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({result : "UserNotFound."})
    }))
  });
};

// Delete a user with the specified id in the request
exports.delete = (event, context, callback) => {
  context.callbackWaitsForEmptyEventLoop = false;

  connectToDatabase()
  .then(() => {
    User.findOneAndUpdate({userId: event.pathParameters.id},JSON.parse(event.body), {new: true})
    .then(user =>  callback(null, {
      statusCode: 200,
      body: JSON.stringify({result: "deleted"})
      // console.log(JSON.stringify(users));
    })).catch(err => callback(null, {
      statusCode: 404,
      headers: { 'Content-Type':  'text/plain' },
      body: JSON.stringify({result: "User Not Ffound."})
    }));
  });
};
