const mongoose = require('mongoose');
const CreditCardSchema = mongoose.Schema({
    vaultID: String,
      creditCardNumber: String,
      creditCardName: String,
      creditCardNickname: String,
      creditCardType: String,
      creditCardExpMonth: String,
      creditCardExpYear: String,
      creditCardCvv: String,
      userId: String,
      isActive: Boolean
}, {
    timestamps: true
});
module.exports = mongoose.model('creditcards', CreditCardSchema);
