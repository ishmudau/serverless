'use strict';

require('dotenv').config({ path: './variables.env' });

const connectToDatabase = require('./db');
const CreditCard = require('../models/creditCard.model.js');
const User = require('../models/User.model.js');
const Promise = require('promise');
const Joi = require('joi');
const https = require('https');
var Request = require('request');
const Config = require('../config/creditConfig.js');
const soap = require('soap');
var _ = require('underscore');


function addCard (cardNumber, cardExpiryDate) {

     const args = {
         'CardVaultRequest': {
             'Account': {
                 'PayGateId': Config.PAYGATE_ID,
                 'Password': Config.PAYGATE_PASSWORD
             },
             'CardNumber': cardNumber,
             'CardExpiryDate': cardExpiryDate
         }
     };

     return new Promise((resolve, reject) => {
         soap.createClient(Config.PAYGATE_URL, { wsdl_options: { gzip: true } }, function (err, client) {
             if (err) {
                 reject(Config.createError("Could not create SOAP client"));
             }
             client.SingleVault(args, function (err, result) {
                 if (err) {
                     reject(Config.createError("Error back from single vault"));
                 }
                 else if (result.CardVaultResponse == null || result.CardVaultResponse.Status == null || result.CardVaultResponse.Status.VaultId == null) {
                     reject({msg: Config.createError("Incorrect response from single vault: " + result.CardVaultResponse.Status.ResultDescription)});
                 }
                 resolve(result.CardVaultResponse);
             });
         });
     });
 }; // module.exports = {addCard: addCard};

//Split card number
function splitCardNumber(cardNumber) {
    var newCardNumber = cardNumber.substring(16, 12);
    var newCardNumber1 = "XXXX XXXX XXXX ";
    return newCardNumber1.concat(newCardNumber);
}

function combineMonthAndYear(month, year) {
    return month.concat(year);
}

//Create and Save a new user
exports.create = (event, context, callback) => {
   // Validate request
   context.callbackWaitsForEmptyEventLoop = false;

   connectToDatabase()
   .then(() => {

     cardmonthyear = combineMonthAndYear(event.body.creditCardExpMonth , event.body.creditCardExpYear);
      var cardPromise = addCard ( event.body.creditCardNumber, cardmonthyear);
      //var vaultId = "";

      cardPromise.then(function (results) {
          // console.log("Results:",results);
          vaultId = _.map(results,  function(status) {
              return status.VaultId;
          });
          // Create a creditCard
          const creditCard = new CreditCard({
              vaultID: vaultId,
              creditCardNumber: splitCardNumber(event.body.creditCardNumber),
              creditCardName: event.body.creditCardName,
              creditCardExpYear: "",
              creditCardExpMonth: "",
              creditCardCvv: event.body.creditCardCvv,
              creditCardType: event.body.creditCardType,
              creditCardNickname: event.body.creditCardNickname,
              userId: event.body.userId
          });
          // Save creditCard in the database
          creditCard.save()
          .then(data => {
            const response = {
                  statusCode: 200,
                  body: JSON.stringify({
                  message: 'success',
                  input: event,
                });
                callback(null, response);

          }).catch(err => {
            const response = {
                  statusCode: 500,
                  body: JSON.stringify({
                  message: 'CouldNotAddCard',
                  input: event,
                });
                callback(null, response);
          });
      })
      // addCard ( event.body.creditCardNumber, event.body.creditCardExpYear);
      // Validate eventuest
      if(event.body == null) {
        const response = {
              statusCode: 404,
              body: JSON.stringify({
              message: 'EmptyBody',
              input: event,
            });
            callback(null, response
      }
    }
  });
};

// Retrieve and return all creditCards from the database.
exports.findAll = (req, res) => {
  context.callbackWaitsForEmptyEventLoop = false;

  connectToDatabase()
    .then(() => {
      CreditCard.find()
      .then(creditCards => {
          const response = {
                statusCode: 200,
                body: JSON.stringify({
                message: creditCards,
                input: event,
              });
              callback(null, response);

      }).catch(err => {
          res.status(500).send({
              message: err.message || "Some error occurred while retrieving creditCards."
          });
      });
    });
};

// Find a single creditCard with a creditCardId
exports.findOne = (req, res) => {
  context.callbackWaitsForEmptyEventLoop = false;

  connectToDatabase()
    .then(() => {

      CreditCard.find({userId:req.params.userId})
      .then(creditCard => {
          if(!creditCard) {
              return res.status(404).send({
                  message: "creditCard not found with userId " + req.params.userId
              });
          }
          res.send(creditCard);
      }).catch(err => {
          if(err.kind === 'ObjectId') {
              return res.status(404).send({
                  message: "creditCard not found with userId " + req.params.userId
              });
          }
          return res.status(500).send({
              message: "Error retrieving creditCard with userId " + req.params.userId
          });
      });
    });
};

//uppdate card nickname
function updateCardNickName(creditCardNickname, creditCardId, res) {
  context.callbackWaitsForEmptyEventLoop = false;

  connectToDatabase()
    .then(() => {
      CreditCard.findByIdAndUpdate(creditCardId, {
          creditCardNickname: creditCardNickname,
      },{new: true})
      .then(creditCard => {
          console.log("Credit Card", creditCard)
          if(!creditCard) {
          res.send(JSON.stringify({ message: "error" }));
        }else{
          res.send(JSON.stringify({ message: "success" }));
        }
      }).catch(error => {
        console.log(error);
      });
    });
}

// Update a creditCard identified by the creditCardId in the request
exports.update = (req, res) => {
    updateCardNickName(req.body.creditCardNickname, req.params.creditCardId, res);
};

// Delete a creditCard with the specified creditCardId in the request
exports.delete = (event,context, callback) => {
  context.callbackWaitsForEmptyEventLoop = false;

  connectToDatabase()
    .then(() => {
      CreditCard.findByIdAndRemove(event.params.creditCardId)
      .then(creditCard => {
          if(!creditCard) {
              return res.status(404).send({
                  message: "creditCard not found with id " + event.params.creditCardId
              });
          }
          res.send({message: "creditCard deleted successfully!"});
      }).catch(err => {
          if(err.kind === 'ObjectId' || err.name === 'NotFound') {
              return res.status(404).send({
                  message: "creditCard not found with id " + event.params.creditCardId
              });
          }
          return res.status(500).send({
              message: "Could not delete creditCard with id " + event.params.userId
          });
      });
    });
};
