'use strict';

require('dotenv').config({ path: './variables.env' });

const connectToDatabase = require('./db');
const Meter = require('./models/Meter');
var _ = require('underscore');
var meterValidator = require('./meterValidator.controller.js');

function splitMeterNumber(meterNumber) {
    var newMeterNumber = meterNumber.substring(4, 0);
    if (newMeterNumber == "0121") {return "VX";}
    return "CP";
}

//is the assigned to a user?
function meterAlreadyAssignedToUser(userId, meterNumber, context) {
  console.log("User ID: ",userId, " meter Number: ", meterNumber);

  return new Promise(function(resolve, reject) {
    context.callbackWaitsForEmptyEventLoop = false;

    connectToDatabase()
    .then(() => {
        var meters = [];
          Meter.find({userId: userId, number: meterNumber})
          .then(userWithMeter => {
              if(userWithMeter)/*exist*/ {
                console.log("exist meter for user", userWithMeter)

                // console.log(userWithMeter);
                var usermeterNumber = _.map(userWithMeter, function(userdata) {
                  meters.push({
                    meterNumber : userdata.number,
                    isActive: userdata.isActive,
                    isBlocked: userdata.isBlocked
                  });
                });

                var filteredMeter = _.pick(meters, function(value, key) {
                  return value.number == meterNumber;
                });

                var flatObject = _.flatten(_.map(filteredMeter));

                if(flatObject.length > 0) { //user has the meter all we need is to update or reject
                  var action = ""
                  var meterData = _.map(flatObject,  function(meter) {
                    if(meter.isBlocked) {//meter is blocked
                      resolve('blocked'); //blocked by admin
                    }
                    if(!meter.isActive  && !meter.isBlocked) {
                      doMeterActivateState(meterNumber, true, context);
                      resolve('actived');//activated by user, we dont need todo anything here
                    }
                    if(meter.isActive == true && meter.isBlocked == false) {
                        resolve('AlreadyAssigned');
                    }
                    else{
                      console.log("1");
                      resolve('addNewMeter'); //so we add new meter becuase the user has not been inserted into th table as yet
                    }
                  })
              } else {
                console.log("2");
               resolve('addNewMeter'); //so we add new meter becuase the user has not been inserted into th table as yet
            }
          }else {
            console.log("Meter dont exist for user");
           resolve('addNewMeter'); //so we add new meter becuase the user has not been inserted into th table as yet
          }
          });
        });
    });
}

function doMeterActivateState(meterNumber, isActive, context) {
  context.callbackWaitsForEmptyEventLoop = false;

  connectToDatabase()
  .then(() => {
          Meter.findOneAndUpdate({meterNumber: meterNumber}, {
            isActive: true, //true use meter, false dont use
          }, {new: true})
          .then(meter => {
            if(!meter) {
              console.log("Meter Not found");
            }
            return JSON.stringify(meter);
            // res.send(meter);
          }).catch(error => {
            console.log("Error: ", error);
          })
    });
}

module.exports.create = (event, context, callback) => {
  context.callbackWaitsForEmptyEventLoop = false;
  connectToDatabase()
  .then(() => {
    // console.log(JSON.parse(event.body));
    var meter = JSON.parse(event.body);
    var vendor = "";
    var agent = "";
    var addmeter = false;
    var type = "";
    var  isVoltexCityPowerMeterPromise = meterValidator.validateVoltexMeter(meter.number);
    var eskomMeterPromise = meterValidator.validateMeter(meter.number);
    var isAvailableMeterPromise = meterAlreadyAssignedToUser(meter.userId, meter.number, context);
    var meterType = splitMeterNumber(meter.number);
    // console.log("Hello there:", meterType);
    isAvailableMeterPromise.then(function(isMeterAddable) {
      if(isMeterAddable == 'actived') {
        // res.send(JSON.stringify({ message: "activated" }));
        statusCode: 500
        // body: 'activated'
      }else if(isMeterAddable == "addNewMeter") {
          isVoltexCityPowerMeterPromise.then(function(voltexMeterPromise) {
            vendor = "CityPower"//set in config file
            agent = "CityPower";
            type = meterType;
            // console.log(meter);
            if(voltexMeterPromise.result == 'ok') {
                console.log(meter);
                var meters = {
                    name: meter.name,
                    number: meter.number,
                    agent : agent,
                    vendor : vendor,
                    type:  type,
                    isActive: true,
                    isBlocked: false,
                    userId: meter.userId
                }
                Meter.create(meters)
               .then(data => callback(null, {
                 statusCode: 200,
                 body: JSON.stringify({result: "success"})

               }))
               .catch(err => callback(null, {
                 statusCode: err.statusCode || 500,
                 body: JSON.stringify({ result: "FailToaddMeter" })
             }))
           }
          })
        }else if(isMeterAddable == "blocked") {
          return JSON.stringify({result: "blocked"})
        }else if(isMeterAddable == "AlreadyAssigned") {
          return JSON.stringify({result: "AlreadyAssigned"})
        } else if(isMeterAddable == "fail") {
          return JSON.stringify({result: "fail"})
        }
      }).catch(err => callback(null,  {
        statusCode: err.statusCode || 500,
        headers: { 'Content-Type': 'text/plain' },
        body: JSON.stringify({result: "FailToaddMeter"})
      }))
    })
}


// Delete a meter with the specified meterId in the request
exports.delete = (event, context, callback) => {
  context.callbackWaitsForEmptyEventLoop = false;
  var meter = {
    isActive: true
  }
  connectToDatabase()
    .then(() => {
      Meter.findByIdAndUpdate({_id: event.pathParameters.meterId}, meter, {new: true})
      .then(meter => callback(null, {
        statusCode: 200,
        body: JSON.stringify({result: "deleted"})
      }))
      .catch(err => callback({
        statusCode: 404,
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({result : "MeterNotFound."})
      }))
    });
};

// Update a meter identified by the meterId in the request
exports.update = (event, context, callback) => {
    context.callbackWaitsForEmptyEventLoop = false;

    connectToDatabase()
      .then(() => {
        Meter.findByIdAndUpdate({_id: event.pathParameters.meterId}, JSON.parse(event.body),{new: true})
        .then(meter => callback(null, {
          statusCode: 200,
          body: JSON.stringify(meter)
        }))
        .catch(err => callback(null, {
          statusCode: 404,
          headers: { 'Content-Type': 'application/json' },
          body: JSON.stringify({result : "MeterNotFound."})
        }))
      })
};

// Retrieve and return all meters from the database.
exports.findAll = (event, context, callback) => {
  context.callbackWaitsForEmptyEventLoop = false;

  connectToDatabase()
    .then(() => {
      Meter.find()
      .then(meters => callback(null, {
        statusCode: 200,
        body: JSON.stringify(meters)
      }))
      .catch(err => callback(null, {
        statusCode: err.statusCode || 500,
        body: 'CouldNotGetMeters'
      }))
    });
};

// Find All meters with a userId
exports.findMetersForUser = (event, context, callback) => {
  // console.log("Getting Meters for User.....")
  context.callbackWaitsForEmptyEventLoop = false;

  connectToDatabase()
    .then(() => {
      Meter.find({userId: event.pathParameters.userId})
        .then(meters => callback(null, {
            statusCode: 200,
            body: JSON.stringify(meters)
        }))
        .catch(err => callback(null, {
          statusCode: 200,
          headers: {'Content-Type': 'application/json' },
          body : 'NoMeterFound'
        }))
      })
};

module.exports.getAll = (event, context, callback) => {
  context.callbackWaitsForEmptyEventLoop = false;

  connectToDatabase()
    .then(() => {
      Meter.find()
        .then(meters => callback(null, {
          statusCode: 200,
          body: JSON.stringify(meters)
        }))
        .catch(err => callback(null, {
          statusCode: err.statusCode || 500,
          headers: { 'Content-Type': 'application/json' },
          body: 'Could not fetch the meters.'
        }))
    });
};
