'use strict'

// const test = false
//     , local = false
//     , fs = require('fs')
//     , config = {}
//     , path = require('path')
//
// config.mongoose = test && local
//     ? { host: 'localhost',
//         port : 27017 }
//     : test && !local
//         ? { host : 'mongodb://184.73.255.120'
//             port : 27017,
//             ssl : {"ca": fs.readFileSync(`${path.resolve(__dirname)}/mongodb.pem`)}
//           }
//     : !test && !local
//           ? { host : 'mongodb://184.73.255.120'
//               port : 27017,
//               ssl : {"ca": fs.readFileSync(`${path.resolve(__dirname)}/mongodb.pem`)}
//             }
//     : {};
//
// module.exports = config;
// //
// // # DB=mongodb://<user>:<test>184.73.255.120:27017/mobile_dev
//
var fs = require('fs')
     , mongoose = require('mongoose')
     , mongoUri = 'mongodb://184.73.255.120:27017'
     , mongoOpt = {
         "server": {
           "sslValidate": true,
           "ssl": fs.readFileSync('./mongodb.pem'),
         }
       }
     ;

mongoose.connect(mongoUri, mongoOpt);
