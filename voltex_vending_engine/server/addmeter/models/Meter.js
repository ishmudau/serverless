const mongoose = require('mongoose');

const MeterSchema = mongoose.Schema({
    name:  String,
    number: String,
    agent: String,
    type: String,
    vendor: String,
    isBlocked: Boolean,
    isActive: Boolean, //true use meter, false dont use
    userId: String //a meter belongs to a person
}, {
    timestamps: true
});

module.exports = mongoose.model('meters', MeterSchema);
