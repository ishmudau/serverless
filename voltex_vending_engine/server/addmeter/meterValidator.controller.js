const Promise = require('promise')
const Joi = require('joi')
const https = require('https')
var Request = require('request')
var _ = require('underscore');


//does the meter exist? Eskom check only...
exports.validateMeter = (meterNumber) => {
  return new Promise (function(resolve, reject) {
    Request.post({
      "headers": { "content-type": "application/json" },
      "url": "http://154.0.174.73:8080/rest/TBSAgentServices/electricity/reprintLastThree",
      "body": JSON.stringify({
          "agentID": "12716140",
          "meterNumber": meterNumber
      })
    }, (error, response, body) => {
      if(error) {
        reject(error);
      }else{
        resolve(JSON.parse(body));
      }
    })
  })
}

// //
exports.validateVoltexMeter = (meterNumber) => {
  const input = {
          "request_id": "CP1", //update to config file
          "request_type": "CP",
          "payload": {
              "meter_number": meterNumber,
              "amount": 1,
              "tendered": 1,
              "api": 1
          }
      };

  return new Promise (function(resolve, reject) {
    Request.post({
      "headers": { "content-type": "application/json" },
      "url": "http://emsvendapi-env-1.zrr9zcmtcv.us-east-1.elasticbeanstalk.com/api/vend",
      "body": JSON.stringify({
        "request_id": "CP1", //update to config file
        "request_type": "CP",
        "payload": {
            "meter_number": meterNumber,
            "amount": 1,
            "tendered": 1,
            "api": 1
          }
      })
    }, (error, response, body) => {
      if(error) {
        reject(error);
      }else{
        resolve(JSON.parse(body));
      }
    })
  })
}
