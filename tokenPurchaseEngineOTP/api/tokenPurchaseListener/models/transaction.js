const mongoose = require('mongoose');

//create a cost collection

const TransactionLogsSchema = mongoose.Schema({
        cPReceiptNumber:  String ,
        costs: {} ,
        credit: Number ,
        date: { type: Date, default: Date.now },
        debit: Number ,
        tokenId:  String,
        kWh:  String ,
        meterId:  String ,
        meterNumber:  String ,
        meterType:  String ,
        agent: String,
        paygateReferenceNumber:  String ,
        paygateRequestId:  String ,
        paygateTransactionDescription:  String ,
        paygateTransactionStatus:  String ,
        paymentMethod:  String ,
        stsToken:  String ,
        transactionType:  String ,
        userId:  String
    }, {
        timestamps: true
});

module.exports = mongoose.model('transactions', TransactionLogsSchema);
