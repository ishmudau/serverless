'use strict'

require('dotenv').config({ path: './variables.env' });

const connectToDatabase = require('./db');
const Transaction = require('./models/transaction');
var _ = require('underscore');
const Promise = require('promise');

module.exports.listener = (event, context, callback) => {

    return new Promise(function(resolve, reject) {
        context.callbackWaitsForEmptyEventLoop = false;
        connectToDatabase()
          .then(() => {
            Transaction.find({tokenId: event.pathParameters.tokenId})
            .then(transaction => callback(null, {
              statusCode: 200,
              body: JSON.stringify(transaction)
         }))
            .catch (err => callback(null, {
                statusCode: err.statusCode || 500,
                headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify({result : "No Transaction"})
            }));
        })
    })
};

 // HANDLE.listener(event)
 //     .then(user => console.log('userchange: ', user))
 //     .catch(err => console.log('listener error: ', err));
