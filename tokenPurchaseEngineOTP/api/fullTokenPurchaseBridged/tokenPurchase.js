'use strict'

// NOTE: THIS ONLY WORKS FOR PURCHASE BY CREDIT CARD. IT DOES NOT WORK FOR EWALLET.

require('dotenv').config({ path: './variables.env' });

const connectToDatabase = require('./db');
const Notification = require('./models/notification');
const Transaction = require('./models/transaction');
const HpsVending = require('./hpsVending');
const CityPowerVending = require('./cityPowerVending');

var _ = require('underscore');
const Request = require('request');
const uuid = require('uuid');
const request = require('request');
const R = require('ramda');
const moment = require('moment-timezone');

//get transaction
const getTransaction = (input, context, callback) => {
  return new Promise(function(resolve, reject) {
      context.callbackWaitsForEmptyEventLoop = false;
      connectToDatabase()
      .then(() => {
          Transaction.find({ paygateRequestId: `${input[0].paygateRequestId}` })
          .then(transaction => {
            const events = Object.assign({}, {transaction: transaction}, {input: input}, {transactions: []});
            return !transaction
                ? reject({msg: 'Could not get user Transaction'})
                : resolve(events);
          }).catch(err => {
              return reject(err)
          })
      })
  })
};

const transactionInsert = (object) => {
  context.callbackWaitsForEmptyEventLoop = false;
  connectToDatabase()
  .then(() => {
      Transaction.create(object)
      .then(data => callback(null, {
          statusCode: 200,
          body: JSON.stringify({result: "success"})
      }))
      .catch(err => callback(null, {
          statusCode: err.statusCode || 500,
          body: JSON.stringify({ result: "FailToaddUser" })
      }))
  })
};

const electricityToken = (input, context, callback) => {
  //check which vendor to use then call the class
  return new Promise(function(resolve, reject) {
    var meterAgent = input;
    if(input.transaction[0].agent == "hps" || input.transaction[0].agent == 'hps') {
      console.log("We are vending Hps ");
      HpsVending.startVending(input, context, callback)
      .then(data => {
        resolve(data);
      })
    }else {
      console.log("We are vending cityPower");
      CityPowerVending.startVending(input, context, callback)
      .then(data => {
        console.log("Hey we back here", data);
        resolve(data);
      })
    }
  })
};

exports.tokenPurchase = (event, context, callback) => {
  return new Promise(function(resolve, reject) {
    getTransaction(event, context, callback)
    .then(input => {
      electricityToken(input, context, callback)
      .then(result => {
        console.log("result: ",result);
        resolve(input)
      })
    })
  })
};

// module.exports = tokenPurchase;
