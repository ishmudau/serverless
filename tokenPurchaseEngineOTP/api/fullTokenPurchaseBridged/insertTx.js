'use strict'

require('dotenv').config({ path: './variables.env' });
const connectToDatabase = require('./db');
const Notification = require('./models/notification');
const Transaction = require('./models/transaction');
const PrepurchaseInfo = require('./models/prepurchaseInfo');
var _ = require('underscore');
const uuid = require('uuid');
const request = require('request');
const R = require('ramda');
const moment = require('moment-timezone');

const transactionDescriptions = [
    'Not Done'
    , 'Approved'
    , 'Declined'
    , 'Cancelled'
    , 'Cancelled'
    , 'Received by PayGate'
    , 'Settlement Voided'
];

const getTransactionInfo = (event, context, callback) => {
  return new Promise(function(resolve, reject) {
    context.callbackWaitsForEmptyEventLoop = false;
    connectToDatabase()
    .then(() => {
      PrepurchaseInfo.find({ paygateRequestId: `${event.paygateRequestID}` })
      .then(prepurchaseInfo => {
            if(prepurchaseInfo) {
            let response = Object.assign(prepurchaseInfo, {transactionStatus: `${event.transactionStatus}` });
            console.log(response);
            resolve(response)
          }else {
            reject('NotGoneHappen')
          }
      })
    })
  })
};

const createTransaction = (input) => {
    // console.log("Our input", input);
    const debit = 0;
    const credit = input[0].amount;
    const id = uuid();

    const transaction = {
          userId: input[0].userId
        ,  date: moment.tz("Africa/Johannesburg").format('lll')
        ,  paymentMethod: 'credit'
        ,  transactionType: 'paygate'
        ,  debit: debit
        ,  credit: credit
        ,  paygateRequestId: input[0].paygateRequestId
        ,  tokenId : input[0].tokenId
        ,  vendor: input[0].vendor
        ,  agent: input[0].agent
        ,  paygateReferenceNumber: input[0].paygateMerchantOrderId
        ,  cPReceiptNumber: null
        ,  meterId: input[0].meterId
        ,  meterNumber: input[0].meterNumber
        , meterAgent: input[0].meterAgent
        ,  meterType: input[0].meterType
        ,  stsToken: null
        ,  kWh: null
        ,  costs: []
        ,  paygateTransactionStatus: input.transactionStatus
        ,  paygateTransactionDescription: transactionDescriptions[input.transactionStatus]
        , payload: {}
    }
    return Promise.resolve(transaction);
};

const saveTransaction = (transaction, context, callback) => {
  // console.log("paygateRequestID: ", transaction.paygateRequestId);
  return new Promise(function(resolve, reject) {
    context.callbackWaitsForEmptyEventLoop = false;
    connectToDatabase()
    .then(() => {
      Transaction.find({ paygateRequestId: transaction.paygateRequestId }) //This is to avoid duplicate transactions
      .then(foundTransaction => {
        if(foundTransaction.length > 0) {
          // console.log("Our Transaction: ", foundTransaction);
          resolve(foundTransaction);
        }else{
          Transaction.create(transaction)
          .then(data => {
            // console.log("insertted: ", data);
            resolve(data)
          })
          .catch(err => {
            // console.log("Esish could not add");
            reject(err)
          })
        }
      })
    })
  })
};

exports.insertTx = (event, context, callback) => {
    return new Promise(function(resolve, reject) {
        context.callbackWaitsForEmptyEventLoop = false;
        getTransactionInfo(event, context, callback)
        .then(input => {
          createTransaction(input)
          .then(tx => {
            saveTransaction(tx,context, callback)
            .then(res => {
              resolve(input)
            })
          })
        })
        .catch(err => {
            return Promise.reject(err);
        })
    })
};
