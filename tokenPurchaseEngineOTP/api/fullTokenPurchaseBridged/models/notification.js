const mongoose = require('mongoose');

const NotificationSchema = mongoose.Schema({
        date: { type: Date, default: Date.now },
        isRead: Boolean ,
        message:  String,
        stsToken:  String ,
        title:  String ,
        userId:  String ,
        id:  String
    }, {
        timestamps: true
});

module.exports = mongoose.model('notifications', NotificationSchema);
