const mongoose = require('mongoose');

const PrepurchaseInfoSchema = mongoose.Schema({
    amount:  String,
    meterId: String,
    meterNumber: String,
    agent: String,
    meterType: String,
    vendor: String,
    paygateMerchantOrderId: String,
    paygateRequestId: String, //true use meter, false dont use
    userId: String, //a meter belongs to a person
    paymentMethod: String,
    tokenId: String,
    email: String
}, {
    timestamps: true
});

module.exports = mongoose.model('prepurchase_infos', PrepurchaseInfoSchema);
