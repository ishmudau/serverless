'use strict'

const response = require('cfn-response');
const AWS = require('aws-sdk');
const _ = require('underscore');
const Promise = require('promise');

const email = require('./email');
const insertTx = require('./insertTx');
const tokenPurchase = require('./tokenPurchase');
const translateResponse = require('./translateResponse');

const { prop, head, pipeP } = require('ramda');
const res = x => Promise.resolve(x)
const log = x => { console.log(x); return x; };

module.exports.subscription = (event, context, callback) => {
  // console.log("HELLO There");
  let records = _.map(event, function(recordsData) {
    if(recordsData[0].Sns.Message != 'undefined' || recordsData[0].Sns.Message != null) {
      console.log(recordsData[0].Sns.Message);
      translateResponse(recordsData[0].Sns.Message)
      .then(input => {
        // console.log("Hey there: ", input);
        insertTx.insertTx(input, context, callback)
        .then(insertInput => {
          // console.log("HEY There: ");
          if(input.transactionStatus === "1" || input.transactionStatus === '1' ) {
            tokenPurchase.tokenPurchase(insertInput, context, callback)
            .then(insertedTrans => {
              console.log("lets send the email",insertedTrans);
              email.email(insertedTrans)
            }).catch(err => callback(null, {
              statusCode: err.statusCode || 500,
              headers: {'Content-Type': 'application/json' },
              body : JSON.stringify({result : "TransactionError"})
            }))
          }else{
            console.log("We can not do it");
            return Promise.reject({err: 'paygate transaction failure'})
          }
        })
      })
    }
  })
}
