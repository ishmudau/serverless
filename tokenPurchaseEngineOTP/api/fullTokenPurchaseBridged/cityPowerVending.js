require('dotenv').config({ path: './variables.env' });

const connectToDatabase = require('./db');
const Notification = require('./models/notification');
const Transaction = require('./models/transaction');
var _ = require('underscore');

const Request = require('request');
const uuid = require('uuid');
const request = require('request');
const R = require('ramda');
const moment = require('moment-timezone');

const fetchToken = event => {
    const id = event.transaction[0].tokenId;
    const meter = event.transaction[0].meterNumber;

    return new Promise (function(resolve, reject) {
      Request.post({
        "headers": { "content-type": "application/json" },
        "url": "http://vend-env.us-east-1.elasticbeanstalk.com/api/vend",
        "body": JSON.stringify({
          "request_id": id, //update to config file
          "request_type": "CP",
          "payload": {
              "meter_number": meter,
              "amount": 1,
              "tendered": 1,
              "api": 5
            }
        })
      }, (error, response, body) => {
        if(error) {
          reject(error);
        }else{
          let response = Object.assign({cPResult: body}, { transaction: event.transaction[0] });
          console.log("Results: ", response);
          resolve(response);
        }
      })
    })
};

const createTransaction = (event, context, callback) => {
    var payloadData = JSON.parse(event.cPResult);
    var transactionData = event.transaction;
    const credit = 0;
    const debit = payloadData.payload.amount_tendered;
    const id = transactionData.tokenId;
    const stsToken = payloadData.payload.sts_token;
    const kWh = payloadData.payload.total_units_issued;
    const cPReceipt = payloadData.payload.receipt_num;
    const payload = payloadData.payload;
    const meterNo = transactionData.meterNumber;
    const transactionID = transactionData._id

    const values = [
        payloadData.payload.amount_tendered
        , 0.00
        , 0.00
        , 0.00
        , 0.00
        , 0.00
        , 0.00
        , 0.00
        , payloadData.payload.amount_tendered
        , payloadData.payload.vat_amount
        , payloadData.payload.value_ex_vat
        , payloadData.payload
    ];

    const keys = ['totalAmount', 'creditCharge', 'creditChargeVat', 'tokenCharge', 'tokenChargeVat',
        'resellerCommission', 'inviroCommission', 'commissionVat', 'energy', 'energyVat', 'vendAmount', 'payload'];
    const calc = R.zipObj(keys, values);

    const transaction = {
          userId: transactionData.userId
        , date: moment.tz("Africa/Johannesburg").format('lll')
        , paymentMethod: transactionData.paymentMethod
        , transactionType: 'tokenPurchase'
        , debit: debit
        , credit: credit
        , tokenId: id
        , paygateReferenceNumber: transactionData.paygateReferenceNumber
        , paygateRequestId: transactionData.paygateRequestId
        , cPReceiptNumber: cPReceipt
        , meterId: transactionData.meterId
        , meterNumber: transactionData.meterNumber
        , meterType: transactionData.meterType
        , stsToken: stsToken
        , payload: payload
        , kWh: kWh
        , costs: calc
        , paygateTransactionStatus: '1'
        , paygateTransactionDescription: 'Approved'
    }

    const notification = {
          userId: transactionData.userId
        ,  id: uuid()
        ,  title: "Token Purchase"
        ,  date: moment.tz("Africa/Johannesburg").format('lll')
        ,  message: `You have purchased ${kWh} kWh for ${payloadData.payload.amount_tendered}.00 for meter: ${meterNo}. Your STS Token number is ${stsToken.substring(0,4)} ${stsToken.substring(4,8)} ${stsToken.substring(8,12)} ${stsToken.substring(12,16)} ${stsToken.substring(16,20)}.`
        ,  stsToken: stsToken
        ,  isRead: false
    }
    console.log('Voltex transaction log: ', transaction, 'User notification Log: ', notification);
    return new Promise(function(resolve, reject) {
      context.callbackWaitsForEmptyEventLoop = false;
      connectToDatabase()
      .then(() => {
        Transaction.findOneAndUpdate({paygateRequestId: `${transactionData.paygateRequestId}` }, {
            cPReceiptNumber: cPReceipt
          , stsToken: stsToken
          , costs: calc
          , kWh: kWh
        }, {upsert: true, new: true})
       .then(data => {
           Notification.create(notification)
           .then(noteData => {
             console.log("Done saving: ",data);
             resolve(data);
           })
           .catch(notificationError => {
             reject("FailToaddNotification" )
         })
       })
       .catch(err => callback(null, {
         statusCode: err.statusCode || 500,
         body: JSON.stringify({ result: "FailToaddTransaction" })
       }))
      })
    })
};

module.exports.startVending = (input, context, callback) => {
  return new Promise(function(resolve, reject) {
    fetchToken(input)
    .then(data => {
      createTransaction(data, context, callback)
      .then(datas => {
        console.log("Hey There: ", datas);
        resolve(datas);
      }).catch(err => {
        console.log("error");
        reject(err);
      })
    })
  })
}
