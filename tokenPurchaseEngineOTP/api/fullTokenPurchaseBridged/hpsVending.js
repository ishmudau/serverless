require('dotenv').config({ path: './variables.env' });

const connectToDatabase = require('./db');
const Notification = require('./models/notification');
const Transaction = require('./models/transaction');
var _ = require('underscore');

const Request = require('request');
const uuid = require('uuid');
const request = require('request');
const R = require('ramda');
const moment = require('moment-timezone');

const fetchToken = event => {
    const id = event.transaction[0].tsokenId;
    const meter = event.transaction[0].meterNumber;
    const agent = event.transaction[0].agent;
    const amount = event.transaction[0].amount;

    return new Promise (function(resolve, reject) {
        var auth = 'Basic ' + new Buffer("voltex.test:nGNCipTLLX3LV929uhAd").toString('base64');
        Request.post({
          "headers": { "Authorization" : auth, "content-type": "application/json" },
          "url": "https://hps-test.kazang.net/switch/v1/elec_vend",
          "body": JSON.stringify({
              "request_reference": id,
              "meter_number": meter,
              "amount": 1,
              "utility": agent,
              "include_nulls": 1
          })
        }, (error, response, body) => {
          if(error) {
            console.log(error);
            reject(error);
          }else {
            let response = Object.assign({cPResult: body}, { transaction: event.transaction[0] });
            console.log("Results: ", response);
            resolve(response);
          }
        })
    })
};

const createTransaction = (event, context, callback) => {
    var payloadData = JSON.parse(event.cPResult);
    var tokenData = payloadData.elec_vend_response;
    var receiptData = payloadData.elec_vend_response;
    var utilityData = payloadData.elec_vend_response;
    var transactionData = event.transaction;
    var credit = 0;
    var debit = tokenData.token_list[0].amount;
    var id = transactionData.tokenId;
    var stsToken = tokenData.token_list[0].number;
    var kWh = tokenData.token_list[0].units;
    var cPReceipt = receiptData.receipt_details.receipt_number; //get elsewhere
    var payload = payloadData.elec_vend_response;
    var meterNo = transactionData.meterNumber;
    var transactionID = transactionData._id;

    const values = [
        receiptData.receipt_details.tender_amount
        , 0.00
        , 0.00
        , 0.00
        , 0.00
        , 0.00
        , 0.00
        , 0.00
        , tokenData.token_list[0].amount
        , utilityData.utility.vat_number
        , utilityData.utility.vat_number
        , payloadData
    ];

    const keys = ['totalAmount', 'creditCharge', 'creditChargeVat', 'tokenCharge', 'tokenChargeVat',
        'resellerCommission', 'inviroCommission', 'commissionVat', 'energy', 'energyVat', 'vendAmount', 'payload'];
    const calc = R.zipObj(keys, values);

    const transaction = {
          userId: transactionData.userId
        , date: moment.tz("Africa/Johannesburg").format('lll')
        , paymentMethod: transactionData.paymentMethod
        , transactionType: 'tokenPurchase'
        , debit: debit
        , credit: credit
        , tokenId: id
        , paygateReferenceNumber: transactionData.paygateReferenceNumber
        , paygateRequestId: transactionData.paygateRequestId
        , cPReceiptNumber: cPReceipt
        , meterId: transactionData.meterId
        , meterNumber: transactionData.meterNumber
        , meterType: transactionData.meterType
        , stsToken: stsToken
        , payload: payload
        , kWh: kWh
        , costs: calc
        , paygateTransactionStatus: '1'
        , paygateTransactionDescription: 'Approved'
    }

    const notification = {
          userId: transactionData.userId
        ,  id: uuid()
        ,  title: "Token Purchase"
        ,  date: moment.tz("Africa/Johannesburg").format('lll')
        ,  message: `You have purchased ${kWh} kWh for ${receiptData.receipt_details.tender_amount} for meter: ${meterNo}. Your STS Token number is ${stsToken.substring(0,4)} ${stsToken.substring(4,8)} ${stsToken.substring(8,12)} ${stsToken.substring(12,16)} ${stsToken.substring(16,20)}.`
        ,  stsToken: stsToken
        ,  isRead: false
    }
    console.log('Voltex transaction log: ', transaction, 'User notification Log: ', notification);
    context.callbackWaitsForEmptyEventLoop = false;
    connectToDatabase()
    .then(() => {
      Transaction.findOneAndUpdate({paygateRequestId: `${transactionData.paygateRequestId}` }, {
          cPReceiptNumber: cPReceipt
        , stsToken: stsToken
        , costs: calc
        , kWh: kWh
      }, {upsert: true, new: true})
     .then(data => {
         Notification.create(notification)
         .then(noteData => {
           return Promise.resolve(data);
         })
         .catch(notificationError => {
           reject("FailToaddNotification" )
       })
     })
     .catch(err => callback(null, {
       statusCode: err.statusCode || 500,
       body: JSON.stringify({ result: "FailToaddTransaction" })
     }))
    })
    return Promise.resolve(transaction);
};

module.exports.startVending = (input, context, callback) => {
  fetchToken(input)
  .then(data => {
    createTransaction(data, context, callback);
  })
}
