'use strict';

const mailcomposer = require('mailcomposer')
const AWS = require('aws-sdk')
const ses = new AWS.SES({region: 'us-east-1'});
const Promise = require('promise');

// const credentials = new AWS.SharedIniFileCredentials({profile: 'webill'});
// AWS.config.credentials = credentials;
// const ses = new AWS.SES({region: 'us-east-1'});

const buildMail = mail =>
    new Promise((resolve, reject) =>
        mail.build((err, msg) =>
            err ? reject(err) : resolve(msg)
        ));

const sesObj = msg =>
    ({
        RawMessage: {Data: msg}
    });

const mailTokenOptions = (event) => {

    const transaction = event.transaction // here we have cost and payload which carries more data
    // console.log('transactionObj: ', transaction, " Date: ", transaction[0].date);
    const input = event.input
    const costData = transaction[0].costs
    // console.log("Costs: ", costData);
    const payloadData = costData.payload;
    // console.log("payload: ", payloadData);

    if(event.vatNumber || event.companyName) {
        return ({
            subject: `Voltex Prepaid Electricity`,
            html: `
    <img src="https://s3.amazonaws.com/vending-app-apk/images-for-emails/8117+-+EMS+-+Customer+App+Note+-+Top+Up+-+REPRO.png">
    <h4>Hello ${payloadData.consumer_name}.</h4>
    <p>Your voucher details are: </p>
    <br>
    <p>Date: ${transaction[0].date}</p>
    <p>Service: Electricity Pre-Paid</p>
    <p>Token: ${transaction[0].stsToken.substring(0,4)} ${transaction[0].stsToken.substring(4,8)} ${transaction[0].stsToken.substring(8,12)} ${transaction[0].stsToken.substring(12,16)} ${transaction[0].stsToken.substring(16,20)}</p>
    <p>Meter Number: ${transaction[0].meterNumber}</p>
    <p>Customer Name: ${payloadData.consumer_name}</p>
    <p>Amount: R ${transaction[0].credit}.00</p>
    <p>Vat: R ${payloadData.vat_amount}</p>
    <p>Total Units: ${transaction[0].kWh} kWh</p>
    <p>VAT Number: ${payloadData.vatNumber}</p>
    <p>Company Name: ${transaction[0].companyName}</p>
    <p>Receipt Number: ${transaction[0].cPReceiptNumber}</p>
    <p>Paygate Reference Number: ${transaction[0].paygateReferenceNumber}</p>
    <br>
    <p>Should you experience any issues using the app, or if you require further information, please feel free to
        contact us at voltexprepaid@voltex.co.za or call us on 082 419 1435.</p>
    <br>
    <p>Regards</p>
    <h4>Voltex Pre-Paid</h4>
    <h4>See more at: www.voltex.co.za</h4>
`,
            from: 'voltexprepaid@voltex.co.za',
            to: "mudauishmael@gmail.com" //input[0].email //[`${transaction.email}`],
        })
    } else {
        return ({
            subject: `Voltex Prepaid Electricity`,
            html: `
    <img src="https://s3.amazonaws.com/vending-app-apk/images-for-emails/8117+-+EMS+-+Customer+App+Note+-+Top+Up+-+REPRO.png">
    <h4>Hello ${payloadData.consumer_name}.</h4>
    <p>Your voucher details are: </p>
    <br>
    <p>Date: ${transaction[0].date}</p>
    <p>Service: Electricity Pre-Paid</p>
    <p>Token: ${transaction[0].stsToken.substring(0,4)} ${transaction[0].stsToken.substring(4,8)} ${transaction[0].stsToken.substring(8,12)} ${transaction[0].stsToken.substring(12,16)} ${transaction[0].stsToken.substring(16,20)}</p>
    <p>Meter Number: ${transaction[0].meterNumber}</p>
    <p>Customer Name: ${payloadData.consumer_name}</p>
    <p>Amount: R ${transaction[0].debit}.00</p>
    <p>Vat: R ${payloadData.vat_amount}</p>
    <p>Total Units: ${transaction[0].kWh} kWh</p>
    <p>Receipt Number: ${transaction[0].cPReceiptNumber}</p>
    <p>Paygate Reference Number: ${transaction[0].paygateReferenceNumber}</p>
    <br>
    <p>Should you experience any issues using the app, or if you require further information, please feel free to
        contact us at voltexprepaid@voltex.co.za or call us on 082 419 1435.</p>
    <br>
    <p>Regards</p>
    <h4>Voltex Pre-Paid</h4>
    <h4>See more at: www.voltex.co.za</h4>
`,
            from: 'voltexprepaid@voltex.co.za',
            to: "mudauishmael@gmail.com" //input[0].email //[`${transaction.email}`]
        })
    }
    console.log("email sent ");
};

const mailTokenOptionsBcc = (event) => {

    const transaction = event.transaction // here we have cost and payload which carries more data
    const input = event.input
    const costData = transaction[0].costs
    const payloadData = costData.payload;

    if(transaction[0].vatNumber || transaction[0].companyName) {
        return ({
            subject: `Voltex Prepaid Electricity (Bcc)`,
            html: `
    <img src="https://s3.amazonaws.com/vending-app-apk/images-for-emails/8117+-+EMS+-+Customer+App+Note+-+Top+Up+-+REPRO.png">
    <h4>Hello ${payloadData.consumer_name}.</h4>
    <p>Your voucher details are: </p>
    <br>
    <p>Date: ${transaction[0].date}</p>
    <p>Service: Electricity Pre-Paid</p>
    <p>Token: ${transaction[0].stsToken.substring(0,4)} ${transaction[0].stsToken.substring(4,8)} ${transaction[0].stsToken.substring(8,12)} ${transaction[0].stsToken.substring(12,16)} ${transaction[0].stsToken.substring(16,20)}</p>
    <p>Meter Number: ${transaction[0].meter}</p>
    <p>Customer Name: ${payloadData.consumer_name}</p>
    <p>Amount: R ${transaction[0].debit}.00</p>
    <p>Vat: R ${payloadData.vat_amount}</p>
    <p>Total Units: ${transaction[0].kWh} kWh</p>
    <p>VAT Number: ${transaction[0].vatNumber}</p>
    <p>Company Name: ${transaction[0].companyName}</p>
    <p>Receipt Number: ${transaction[0].cPReceiptNumber}</p>
    <p>Paygate Reference Number: ${transaction[0].paygateReferenceNumber}</p>
    <br>
    <p>Should you experience any issues using the app, or if you require further information, please feel free to
        contact us at voltexprepaid@voltex.co.za or call us on 082 419 1435.</p>
    <br>
    <p>Regards</p>
    <h4>Voltex Pre-Paid</h4>
    <h4>See more at: www.voltex.co.za</h4>
`,
            from: 'voltexprepaid@voltex.co.za',
            to: ['taryn@invirohub.com', 'kunle@invirohub.com'],
        })
    } else {
        return ({
            subject: `Voltex Prepaid Electricity (Bcc)`,
            html: `
    <img src="https://s3.amazonaws.com/vending-app-apk/images-for-emails/8117+-+EMS+-+Customer+App+Note+-+Top+Up+-+REPRO.png">
    <h4>Hello ${payloadData.consumer_name}.</h4>
    <p>Your voucher details are: </p>
    <br>
    <p>Date: ${transaction[0].date}</p>
    <p>Service: Electricity Pre-Paid</p>
    <p>Token: ${transaction[0].stsToken.substring(0,4)} ${transaction[0].stsToken.substring(4,8)} ${transaction[0].stsToken.substring(8,12)} ${transaction[0].stsToken.substring(12,16)} ${transaction[0].stsToken.substring(16,20)}</p>
    <p>Meter Number: ${transaction[0].meterNumber}</p>
    <p>Customer Name: ${payloadData.consumer_name}</p>
    <p>Amount: R ${transaction[0].debit}.00</p>
    <p>Vat: R ${payloadData.vat_amount}</p>
    <p>Total Units: ${transaction[0].kWh} kWh</p>
    <p>Receipt Number: ${transaction[0].cPReceiptNumber}</p>
    <p>Paygate Reference Number: ${transaction[0].paygateReferenceNumber}</p>
    <br>
    <p>Should you experience any issues using the app, or if you require further information, please feel free to
        contact us at voltexprepaid@voltex.co.za or call us on 082 419 1435.</p>
    <br>
    <p>Regards</p>
    <h4>Voltex Pre-Paid</h4>
    <h4>See more at: www.voltex.co.za</h4>
`,
            from: 'voltexprepaid@voltex.co.za',
            to: ['taryn@invirohub.com', 'kunle@invirohub.com']
        })
    }
    console.log("sending...");
};

const mailBcc = (event) => {
  // console.log("mailBcc: ", event);
  mailcomposer(mailTokenOptionsBcc(event));
}
const mailToken = (event) => {
  // console.log("mailToken: ",event);
  mailcomposer(mailTokenOptions(event));
}
const emailUser = (event) => {
  // console.log("emailUser: ", event);
  Promise.resolve(event)
      .then(event => {-
        // console.log("events: ", event);
        buildMail(mailToken(event))
        .then(sesObj => {

          then(params => {
            // console.log("Params", params);
            ses.sendRawEmail(params).promise();
          })
        })
      })
      // .then(sesObj)
      // .then(params =>
      //     ses.sendRawEmail(params)
      //         .promise());
}

const emailBcc = (event) => {
  Promise.resolve(event)
      .then(ev => buildMail(mailBcc(ev)))
      .then(sesObj)
      .then(params =>
          ses.sendRawEmail(params)
              .promise());
}
exports.email = (event) => {
    // console.log('email event: ', event);
    return emailUser(event)
        .then(() => emailBcc(event));
};

// const test = require('../test.json');

// email(test).then(res => {
//     console.log('res: ', res);
// }).catch(err => console.log('err: ', err));
