'use strict'

const translateResponse = (string) => {
  console.log("payReqId: ", string);
  const payReqId = string.split('=')[1].split('&')[0];
  const transactionStatus = string.split('=')[2].split('&')[0];
  const input = {
        paygateRequestID: payReqId,
        transactionStatus: transactionStatus
  }
  return Promise.resolve(input);
};

module.exports = translateResponse;
