'use strict';

const aws = require('aws-sdk');


module.exports.hello = (event, context, callback) => {

  var lambda = new aws.Lambda()
  var opts = {
    FunctionName: 'fullTokenPurchaseBridged-dev-topicSubscription'
  }
  lambda.invoke(opts, function(err, data) {
      if(err){
        console.log('error : ', err);
        callback(err, null)
      }else if(data) {
        const response = {
          statusCode: 200,
          body: JSON.parse(data)
        }
        // console.log("response: ",response);
        callback(null, response)
      }
  })
};


module.exports.tokenBride = (event, context, callback) => {
  var lambda = new aws.Lambda()
  var opts = {
    FunctionName: 'paygateTokenBridge-dev-create'
  }
  lambda.invoke(opts, function(err, data) {
      if(err){
        console.log('error : ', err);
        callback(err, null)
      }else if(data) {
        const response = {
          statusCode: 200,
          body: JSON.parse(data)
        }
        // console.log("response: ",response);
        callback(null, response)
      }
  })
};

module.exports.paygateOTP = (event, context, callback) => {

  var lambda = new aws.Lambda()
  var opts = {
    FunctionName: 'paygateOTP-dev-create'
  }
  lambda.invoke(opts, function(err, data) {
      if(err){
        console.log('error : ', err);
        callback(err, null)
      }else if(data) {
        const response = {
          statusCode: 200,
          body: JSON.parse(data)
        }
        // console.log("response: ",response);
        callback(null, response)
      }
  })
};
