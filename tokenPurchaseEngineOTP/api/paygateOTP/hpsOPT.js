'use strict'

require('dotenv').config({ path: './variables.env' });

const https = require('https');
const Request = require('request');
const connectToDatabase = require('./db');
const joi = require('joi')
    , soap = require('soap')
    , configs = require('./config')
    , Promise = require('promise');
/*
 sample request:
 ---------------
 {
 "title": "MR",
 "firstName": "Bob",
 "lastName": "Smith",
 "telephone": "0832506785",
 "mobile": "0832506785",
 "email": "simon@brokenkeyboards.com",
 "vaultId": "2e3fe7df-8e26-431a-ad82-4ba0d7df598a",
 "cvv": "999",
 "amount": 100,
 "merchantOrderId": "Inviro-test-123"
 }
*/

function vendorIsAlive(meterNumber) {
   return new Promise (function(resolve, reject) {
     var auth = 'Basic ' + new Buffer("voltex.test:nGNCipTLLX3LV929uhAd").toString('base64');
     Request.post({
       "headers": { "Authorization" : auth, "content-type": "application/json" },
       "url": "https://hps-test.kazang.net/switch/v1/elec_customer_lookup",
       "body": JSON.stringify({
           "meter_number": meterNumber
       })
     }, (error, response, body) => {
       if(error) {
         console.log(error);
         reject(error);
       }else{
         resolve(JSON.parse(body));
       }
     })
   })
}

exports.hpsOTP = (event, context, callback) => {
    context.callbackWaitsForEmptyEventLoop = false;
    var eventBody = JSON.parse(event.body);
    var isSctiveVendorPromise = vendorIsAlive(eventBody.meterNumber);
    isSctiveVendorPromise.then(function(activeVendorPromise) {
    if(activeVendorPromise.response_code == '0' || activeVendorPromise.response_code == "0") {
      const args = {
          'CardPaymentRequest': {
              'Account': {
                  'PayGateId': configs.PAYGATE_ID,
                  'Password': configs.PAYGATE_PASSWORD
              },
              'Customer': {
                  'Title': eventBody.title,
                  'FirstName': eventBody.firstName,
                  'LastName': eventBody.lastName,
                  'Telephone': eventBody.contact,
                  'Mobile': eventBody.contact,
                  'Email': eventBody.email
              },
              'VaultId': eventBody.vaultId,
              'CVV': eventBody.cvv,
              'BudgetPeriod': '0', // budget period is not supported
              'Redirect': {
                'NotifyUrl': 'https://ut5uaiwt5c.execute-api.us-east-1.amazonaws.com/dev/paygateTokenBridge', //'https://1z0iz0yqp0.execute-api.us-east-1.amazonaws.com/latest/paygate-bridge',
                'ReturnUrl': 'https://klxmzsff2i.execute-api.us-east-1.amazonaws.com/dev/getS3Page'
              },
              'Order': {
                  'MerchantOrderId': eventBody.merchantOrderId,
                  'Currency': 'ZAR',
                  'Amount': eventBody.amount
              }
          }
      };

      return new Promise((resolve, reject) => {

          soap.createClient(configs.PAYGATE_URL, { wsdl_options: { gzip: true } }, function (err, client) {
              if (err) {
                  reject({message: 'paygate'});
              }
              client.SinglePayment(args, function (err, result) {
                  if (err) {
                     const response = {
                        statusCode: err.statusCode || 500,
                        body: JSON.stringify({result: err})
                     }
                      callback(null, response);
                  }
                  connectToDatabase()
                  .then(() => callback(null, {
                      statusCode: 200,
                      body: JSON.stringify({result})
                  }))
                  // resolve(result);
              });
          });
      });
    }
    })
};
