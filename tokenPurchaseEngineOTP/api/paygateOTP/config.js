var boom = require('boom'),
    DEBUG = true;

config = {
    PAYGATE_URL : 'https://secure.paygate.co.za/PayHOST/process.trans?wsdl',
    PAYGATE_ID : '33421021200', //test: '10011072130'
    PAYGATE_PASSWORD : 'eMsInv1r0t3l', // test: 'test'
    createError : function(msg) {
        if(DEBUG) {
            return boom.notImplemented(msg);
        }
        else {
            // this will intentionally not return anything useful to caller for security reasons,
            //  but will log msg
            return boom.badImplementation(msg);
        }
    }
}

module.exports = config;
