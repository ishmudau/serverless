'use strict'

require('dotenv').config({ path: './variables.env' });

const CitypowerOTP = require('./cityPowerOTP')
const HpsOpt = require('./hpsOPT')
const https = require('https');
const Request = require('request');
const connectToDatabase = require('./db');
const joi = require('joi')
    , soap = require('soap')
    , configs = require('./config')
    , Promise = require('promise');

exports.create = (event, context, callback) => {
    // context.callbackWaitsForEmptyEventLoop = false;
    var eventBody = JSON.parse(event.body);
    if(eventBody.agent == 'hps' || eventBody.agent == "hps"){
      HpsOpt.hpsOTP(event, context, callback);
    }else {
      CitypowerOTP.citypowerOTP(event, context, callback);
    }
};
