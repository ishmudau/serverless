'use strict';

require('dotenv').config({ path: './variables.env' });

const connectToDatabase = require('./db');
const PrepurchaseInfo = require('./models/prepurchaseInfo');
var _ = require('underscore');
const Promise = require('promise');

// const insertTransactionInfo = event => {
exports.create = (event, context, callback) => {
    context.callbackWaitsForEmptyEventLoop = false;

    connectToDatabase()
    .then(() => {
      PrepurchaseInfo.create(JSON.parse(event.body))
      .then(prepurchase_info => callback(null, {
            statusCode: 200,
            body: JSON.stringify({result: 'success'})
      }))
      .catch(err =>callback(null, {
          statusCode: err.statusCode || 500,
          headers: { 'Content-Type': 'application/json' },
          body: JSON.stringify({result: err})
      }))
    })
};
