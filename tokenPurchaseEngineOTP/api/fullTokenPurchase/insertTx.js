'use strict'

require('dotenv').config({ path: './variables.env' });

const config = require('./config').rethink
    , r = require('rethinkdb')
    , uuid = require('uuid')
    , moment = require('moment-timezone');

const transactionDescriptions = [
    'Not Done'
    , 'Approved'
    , 'Declined'
    , 'Cancelled'
    , 'Cancelled'
    , 'Received by PayGate'
    , 'Settlement Voided'
];

const getTransactionInfo = (info, conn) => {
    return r.db('invirohub_mobile').table('prepurchase_info').get(info.paygateRequestID)
        .run(conn)
        .then(input => {
             if(!input) {
                 return Promise.reject({msg: `Could not get transaction info`})
             } else {
                 let res = Object.assign(input, { transactionStatus: info.transactionStatus});
                 return Promise.resolve(res);
             }
        });
};

const createTransaction = (input) => {

    const debit = 0;
    const credit = input.amount;
    const id = uuid();

    const transaction = Object.assign({}
        , { id: id }
        , { userId: input.userId }
        , { date: moment.tz("Africa/Johannesburg").format('lll') }
        , { paymentMethod: 'credit' }
        , { transactionType: 'paygate'}
        , { debit: debit }
        , { credit: credit }
        , { paygateRequestId: input.paygateRequestId }
        , { paygateReferenceNumber: input.paygateMerchantOrderId }
        , { cPReceiptNumber: null }
        , { meterId: null }
        , { meterNumber: null }
        , { meterType: null }
        , { stsToken: null }
        , { kWh: null }
        , { costs: [] }
        , { paygateTransactionStatus: input.transactionStatus }
        , { paygateTransactionDescription: transactionDescriptions[input.transactionStatus]}
    );

    console.log('Voltex transaction log: ', transaction);

    return Promise.resolve(transaction);
};

const saveTransaction = (transaction, conn) => {
        return r.db('invirohub_mobile').table('transactions')
            .insert(transaction)
            .run(conn)
            .then(res => {
                conn.close();
                return Promise.resolve(res);
            })
            .catch(err => {
                conn.close();
                return Promise.resolve(err)
            });
};

const main = info => {
    return r.connect(config)
        .then(conn => {
            return getTransactionInfo(info, conn)
                .then(input => {
                    return Promise.resolve()
                        .then(() => createTransaction(input))
                        .then(tx => saveTransaction(tx, conn))
                        .then(res => {
                            console.log('saveResult: ', res);
                            return input;
                        });
                })
                .then(result => { conn.close(); return result; })
                .catch(err => { conn.close(); return Promise.reject(err); });
        });

};

module.exports = main;
