'use strict';

require('dotenv').config({ path: './variables.env' });

const mailcomposer = require('mailcomposer')
    , AWS = require('aws-sdk')
    , ses = new AWS.SES({region: 'us-east-1'});

// const credentials = new AWS.SharedIniFileCredentials({profile: 'webill'});
// AWS.config.credentials = credentials;
// const ses = new AWS.SES({region: 'us-east-1'});

const buildMail = mail =>
    new Promise((resolve, reject) =>
        mail.build((err, msg) =>
            err ? reject(err) : resolve(msg)
        ));

const sesObj = msg =>
    ({
        RawMessage: {Data: msg}
    });

const mailTokenOptions = (event) => {

    const transaction = event.user.transactionLogs.filter(t => t.id === event.input.tokenId)[0];
    console.log('transactionObj: ', transaction, transaction.date);

    if(event.user.vatNumber || event.user.companyName) {
        return ({
            subject: `Voltex Prepaid Electricity`,
            html: `
    <img src="https://s3.amazonaws.com/vending-app-apk/images-for-emails/8117+-+EMS+-+Customer+App+Note+-+Top+Up+-+REPRO.png">
    <h4>Hello ${event.user.firstName} ${event.user.lastName}.</h4>
    <p>Your voucher details are: </p>
    <br>
    <p>Date: ${transaction.date}</p>
    <p>Service: Electricity Pre-Paid</p>
    <p>Token: ${transaction.stsToken.substring(0,4)} ${transaction.stsToken.substring(4,8)} ${transaction.stsToken.substring(8,12)} ${transaction.stsToken.substring(12,16)} ${transaction.stsToken.substring(16,20)}</p>
    <p>Meter Number: ${transaction.meter}</p>
    <p>Customer Name: ${event.cPResult.payload.consumer_name}</p>
    <p>Amount: R ${transaction.debit}.00</p>
    <p>Vat: R ${event.cPResult.payload.vat_amount}</p>
    <p>Total Units: ${transaction.kWh} kWh</p>
    <p>VAT Number: ${event.user.vatNumber}</p>
    <p>Company Name: ${event.user.companyName}</p>
    <p>Receipt Number: ${transaction.cPReceiptNumber}</p>
    <p>Paygate Reference Number: ${transaction.paygateReferenceNumber}</p>
    <br>
    <p>Should you experience any issues using the app, or if you require further information, please feel free to
        contact us at voltexprepaid@voltex.co.za or call us on 082 419 1435.</p>
    <br>
    <p>Regards</p>
    <h4>Voltex Pre-Paid</h4>
    <h4>See more at: www.voltex.co.za</h4>
`,
            from: 'voltexprepaid@voltex.co.za',
            to: [`${event.user.email}`],
        })
    } else {
        return ({
            subject: `Voltex Prepaid Electricity`,
            html: `
    <img src="https://s3.amazonaws.com/vending-app-apk/images-for-emails/8117+-+EMS+-+Customer+App+Note+-+Top+Up+-+REPRO.png">
    <h4>Hello ${event.user.firstName} ${event.user.lastName}.</h4>
    <p>Your voucher details are: </p>
    <br>
    <p>Date: ${transaction.date}</p>
    <p>Service: Electricity Pre-Paid</p>
    <p>Token: ${transaction.stsToken.substring(0,4)} ${transaction.stsToken.substring(4,8)} ${transaction.stsToken.substring(8,12)} ${transaction.stsToken.substring(12,16)} ${transaction.stsToken.substring(16,20)}</p>
    <p>Meter Number: ${transaction.meter}</p>
    <p>Customer Name: ${event.cPResult.payload.consumer_name}</p>
    <p>Amount: R ${transaction.debit}.00</p>
    <p>Vat: R ${event.cPResult.payload.vat_amount}</p>
    <p>Total Units: ${transaction.kWh} kWh</p>
    <p>Receipt Number: ${transaction.cPReceiptNumber}</p>
    <p>Paygate Reference Number: ${transaction.paygateReferenceNumber}</p>
    <br>
    <p>Should you experience any issues using the app, or if you require further information, please feel free to
        contact us at voltexprepaid@voltex.co.za or call us on 082 419 1435.</p>
    <br>
    <p>Regards</p>
    <h4>Voltex Pre-Paid</h4>
    <h4>See more at: www.voltex.co.za</h4>
`,
            from: 'voltexprepaid@voltex.co.za',
            to: [`${event.user.email}`]
        })
    }
};

const mailTokenOptionsBcc = (event) => {

    const transaction = event.user.transactionLogs.filter(t => t.id == event.input.tokenId)[0];

    if(event.user.vatNumber || event.user.companyName) {
        return ({
            subject: `Voltex Prepaid Electricity (Bcc)`,
            html: `
    <img src="https://s3.amazonaws.com/vending-app-apk/images-for-emails/8117+-+EMS+-+Customer+App+Note+-+Top+Up+-+REPRO.png">
    <h4>Hello ${event.user.firstName} ${event.user.lastName}.</h4>
    <p>Your voucher details are: </p>
    <br>
    <p>Date: ${transaction.date}</p>
    <p>Service: Electricity Pre-Paid</p>
    <p>Token: ${transaction.stsToken.substring(0,4)} ${transaction.stsToken.substring(4,8)} ${transaction.stsToken.substring(8,12)} ${transaction.stsToken.substring(12,16)} ${transaction.stsToken.substring(16,20)}</p>
    <p>Meter Number: ${transaction.meter}</p>
    <p>Customer Name: ${event.cPResult.payload.consumer_name}</p>
    <p>Amount: R ${transaction.debit}.00</p>
    <p>Vat: R ${event.cPResult.payload.vat_amount}</p>
    <p>Total Units: ${transaction.kWh} kWh</p>
    <p>VAT Number: ${event.user.vatNumber}</p>
    <p>Company Name: ${event.user.companyName}</p>
    <p>Receipt Number: ${transaction.cPReceiptNumber}</p>
    <p>Paygate Reference Number: ${transaction.paygateReferenceNumber}</p>
    <br>
    <p>Should you experience any issues using the app, or if you require further information, please feel free to
        contact us at voltexprepaid@voltex.co.za or call us on 082 419 1435.</p>
    <br>
    <p>Regards</p>
    <h4>Voltex Pre-Paid</h4>
    <h4>See more at: www.voltex.co.za</h4>
`,
            from: 'voltexprepaid@voltex.co.za',
            to: ['taryn@invirohub.com', 'boitumelo@invirohub.com', 'kunle@invirohub.com'],
        })
    } else {
        return ({
            subject: `Voltex Prepaid Electricity (Bcc)`,
            html: `
    <img src="https://s3.amazonaws.com/vending-app-apk/images-for-emails/8117+-+EMS+-+Customer+App+Note+-+Top+Up+-+REPRO.png">
    <h4>Hello ${event.user.firstName} ${event.user.lastName}.</h4>
    <p>Your voucher details are: </p>
    <br>
    <p>Date: ${transaction.date}</p>
    <p>Service: Electricity Pre-Paid</p>
    <p>Token: ${transaction.stsToken.substring(0,4)} ${transaction.stsToken.substring(4,8)} ${transaction.stsToken.substring(8,12)} ${transaction.stsToken.substring(12,16)} ${transaction.stsToken.substring(16,20)}</p>
    <p>Meter Number: ${transaction.meter}</p>
    <p>Customer Name: ${event.cPResult.payload.consumer_name}</p>
    <p>Amount: R ${transaction.debit}.00</p>
    <p>Vat: R ${event.cPResult.payload.vat_amount}</p>
    <p>Total Units: ${transaction.kWh} kWh</p>
    <p>Receipt Number: ${transaction.cPReceiptNumber}</p>
    <p>Paygate Reference Number: ${transaction.paygateReferenceNumber}</p>
    <br>
    <p>Should you experience any issues using the app, or if you require further information, please feel free to
        contact us at voltexprepaid@voltex.co.za or call us on 082 419 1435.</p>
    <br>
    <p>Regards</p>
    <h4>Voltex Pre-Paid</h4>
    <h4>See more at: www.voltex.co.za</h4>
`,
            from: 'voltexprepaid@voltex.co.za',
            to: ['taryn@invirohub.com', 'boitumelo@invirohub.com', 'kunle@invirohub.com']
        })
    }
};

const mailBcc = event =>
    mailcomposer(mailTokenOptionsBcc(event));

const mailToken = event =>
    mailcomposer(mailTokenOptions(event));

const emailUser = (event) =>
    Promise.resolve(event)
        .then(event => buildMail(mailToken(event)))
        .then(sesObj)
        .then(params =>
            ses.sendRawEmail(params)
                .promise());

const emailBcc = (event) =>                         // This should be removed once app is stable
        Promise.resolve(event)
            .then(ev => buildMail(mailBcc(ev)))
            .then(sesObj)
            .then(params =>
                ses.sendRawEmail(params)
                    .promise());

const email = (event) => {
    console.log('email event: ', event);
    return emailUser(event)
        .then(() => emailBcc(event));
};

module.exports = email;


// const test = require('../test.json');

// email(test).then(res => {
//     console.log('res: ', res);
// }).catch(err => console.log('err: ', err));
