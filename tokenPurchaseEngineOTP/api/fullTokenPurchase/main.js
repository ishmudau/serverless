'use strict'

require('dotenv').config({ path: './variables.env' });

const email = require('./email')
    , insertTx = require('./insertTx')
    , tokenPurchase = require('./tokenPurchase')
    , translateResponse = require('./translateResponse');

const main = paygateResponse => {
    return Promise.resolve()
        .then(() => translateResponse(paygateResponse))
        .then(input => insertTx(input))
        .then(input => {
            console.log('input: ', input);
            if(input.transactionStatus === "1") {
                return tokenPurchase(input)
            }
            return Promise.reject({err: 'Paygate transaction failure'})
        })
        .then(event => email(event))
        .catch(err => {
            console.log('rejection err: ', err);
            return Promise.resolve('OK');
        })
        .then(() => 'OK');
};

module.exports = { fullTokenPurchase: main };
