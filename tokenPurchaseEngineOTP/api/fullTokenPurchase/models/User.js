const mongoose = require('mongoose');

const UserSchema = mongoose.Schema({
        id: String,
        userId: String,
        title:  String,
        firstName: String,
        lastName: String,
        idNumber: String,
        email: String,
        contact: String,
        company: String,
        vatNumber: String,
        balance : String,
        credit : String,
        creditLimit: Number,
        isReseller: Boolean,
        monthlyKwh: Number,
        purchaseLimit: Number,
        status: Boolean

    }, {
        timestamps: true
});

module.exports = mongoose.model('users', UserSchema);
