const mongoose = require('mongoose');

const PrepurchaseInfoSchema = mongoose.Schema({
    amount:  String,
    meterId: String,
    agent: String,
    meterType: String,
    vendor: String,
    paygateMerchantOrderId: String,
    paygateRequestId: String, //true use meter, false dont use
    userId: String, //a meter belongs to a person
    paymentMethod: String,
    tokenId: String
}, {
    timestamps: true
});

module.exports = mongoose.model('prepurchase_info', PrepurchaseInfoSchema);
