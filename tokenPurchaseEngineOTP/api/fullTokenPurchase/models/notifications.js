const mongoose = require('mongoose');

const NotificationSchema = mongoose.Schema({
        id: String,
        userId: String,
        date:  String,
        isRead: Boolean,
        message: String,
        stsToken: String,
        title: String

    }, {
        timestamps: true
});

module.exports = mongoose.model('notifications', NotificationSchema);
