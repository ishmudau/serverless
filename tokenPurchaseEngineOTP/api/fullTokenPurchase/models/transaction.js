const mongoose = require('mongoose');

const TransactionLogsSchema = mongoose.Schema({
        cPReceiptNumber:  String ,
        costs: {} ,
        credit: Number ,
        date: { type: Date, default: Date.now },
        debit: Number ,
        tokenId:  String,
        kWh:  String ,
        meterId:  String ,
        meterNumber:  String,
        agent: String,
        meterType:  String ,
        paygateReferenceNumber:  String ,
        paygateRequestId:  String ,
        paygateTransactionDescription:  String ,
        paygateTransactionStatus:  String ,
        paymentMethod:  String ,
        stsToken:  String ,
        tokenId: String,
        transactionType:  String ,
        userId:  String,
        email: String
    }, {
        timestamps: true
});

module.exports = mongoose.model('transactions', TransactionLogsSchema);
