'use strict'

require('dotenv').config({ path: './variables.env' });
const request = require('request');
const restPath = 'http://vending-app-apk-otpprocess.s3-website-eu-west-1.amazonaws.com';

exports.getPage = (event, context, callback) => {
    context.callbackWaitsForEmptyEventLoop = false;
     return new Promise((resolve, reject) => {
         request.get(
             restPath
             , function (error, response, body) {
                 if (!error && response.statusCode === 200) {
                     const response = {
                       statusCode: 200,
                       headers: { 'Content-Type': 'application/json' },
                       body: JSON.stringify(body)
                     }
                     callback(null, response)
                 } else {
                     reject({msg: 'error with http'});
                 }
             }
         )
     });
}
