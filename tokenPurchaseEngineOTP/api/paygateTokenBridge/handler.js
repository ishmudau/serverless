'use strict'

const { dbt, topic } = require('./config');

const AWS = require('aws-sdk');
// const connectToDatabase = require('./db');
// , credentials = new AWS.SharedIniFileCredentials({profile: 'webill'});
// AWS.config.credentials = credentials;
const topicName = 'arn:aws:sns:us-east-1:857881858502:fullTokenPurchaseBridged'
const moment = require('moment')
const timezone = require('moment-timezone');

const tableName = 'paygateTransactions';

const sns = new AWS.SNS({region: 'us-east-1'})
const ddb = new AWS.DynamoDB.DocumentClient({ region: 'eu-west-1' });

const { ifElse, isEmpty, mergeAll } = require('ramda');

const res = x => Promise.resolve(x)
    , rej = x => Promise.reject(x)
    , log = x => { console.log(x); return x; };

const dbPut = param => ddb.put(param).promise()
    , dbFetch = param => ddb.get(param).promise().then(ifElse(isEmpty, _ => false, res));

const fetchID = id => {
    const param = {
        Key: {
            paygateRequestID: id
        },
        TableName: tableName
    };
    return dbFetch(param);
};

const insertID = payObj => {
    const param = {
        Item: mergeAll([payObj, { ttl: moment().format('X') }, { cat: timezone.tz("Africa/Johannesburg").format('lll') }]),
        TableName: tableName
    };
    return dbPut(param)
        .then(x => 'endpoint created | endpoint updated to db')
        .catch(err => 'endpoint created | endpoint failed to update to db');
};

const initiateFullTokenPurchase = (event) => {
    console.log(topicName);
    sns.publish({Message: event, TopicArn: topicName})
        .promise();
};

const translateResponse = (string) => {
    const payReqId = string.split('=')[1].split('&')[0];
    const transactionStatus = string.split('=')[2].split('&')[0];
    return Promise.resolve(
        {
            paygateRequestID: payReqId,
            transactionStatus: transactionStatus
        }
    );
};

exports.main = (event, context, callback) => {
    context.callbackWaitsForEmptyEventLoop = false;
    res(event.body)
        .then(log)
        .then(translateResponse)
        .then(payObj =>
            res(fetchID(payObj.paygateRequestID))
                .then(notEmpty => !notEmpty ? insertID(payObj) : rej('paygateRequestID already exists'))
        )
        .then(log)
        .then(() => initiateFullTokenPurchase(event.body))
        .then(log)
        .catch(log)
        .then(() => callback(null, {
            statusCode: 200,
            body: JSON.parse('OK')
        }))
        .catch(err => callback(null, {
            statusCode: err.statusCode || 500,
            body: JSON.stringify({ result: err })
        }))
}
