'use strict'

const stage = 'dev'
    , table = 'paygateTransactions';

const config = {
    dbt: `prepaidapp.${table}`,
    topic: `arn:aws:lambda:us-east-1:857881858502:function:fullTokenPurchaseBridged-dev-create`
    // topic: 'arn:aws:sns:us-east-1:873491694642:emsinvirohub_prepaidapp_fulltokenpurchasebridge'
};
 // arn:aws:lambda:us-east-1:857881858502:function:fullTokenPurchaseBridged-dev-create
// arn:aws:dynamodb:us-east-1:857881858502:table/paygateTransactions/stream/


module.exports = config;
