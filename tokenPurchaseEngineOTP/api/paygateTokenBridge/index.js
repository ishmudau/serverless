const ApiBuilder = require('claudia-api-builder'),
    AWS = require('aws-sdk');
var api = new ApiBuilder(),
    dynamoDb = new AWS.DynamoDB.DocumentClient();

api.post('/prepaidapp.paygateTransactions', function (request) { // SAVE your icecream
  var params = {
    TableName: 'prepaidapp.paygateTransactions',
    Item: {
        icecreamid: request.body.paygateRequestID,
        name: request.body.paygateTransactionStatus // your icecream name
    }
  }
  return dynamoDb.put(params).promise(); // returns dynamo result
}, { success: 201 }); // returns HTTP status 201 - Created if successful

api.get('/prepaidapp.paygateTransactions', function (request) { // GET all users
  return dynamoDb.scan({ TableName: 'prepaidapp.paygateTransactions' }).promise()
      .then(response => response.Items)
});
