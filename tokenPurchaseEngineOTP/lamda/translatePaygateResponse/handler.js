'use strict'

const translateResponse = (string) => {
    const payReqId = string.split('=')[1].split('&')[0];
    const transactionStatus = string.split('=')[2].split('&')[0];
    return Promise.resolve(
        {
            paygateRequestID: payReqId,
            transactionStatus: transactionStatus
        }
    );
};

module.exports = { translateResponse: translateResponse };
