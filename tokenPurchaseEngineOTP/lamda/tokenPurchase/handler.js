'use strict'

// NOTE: THIS ONLY WORKS FOR PURCHASE BY CREDIT CARD. IT DOES NOT WORK FOR EWALLET.

const uuid = require('uuid')
    , request = require('request')
    , R = require('ramda')
    , config = require('./config').rethink
    , r = require('rethinkdb');

const getUser = (input, conn) => {

    return r.db('invirohub_mobile').table('users').get(input.userId)
        .run(conn)
        .then(profile => {

            const event = Object.assign({}, {user: profile}, {input: input}, {transactions: []});
            return !profile
                ? Promise.reject({msg: `Could not get user profile`})
                : Promise.resolve(event);
        })
        .catch(err => {
            return Promise.reject(err);
        })
};

// cityPowerToken Functions:

const transactionInsert = (object, conn) => {

    return r.db('invirohub_mobile').table('transactions')
        .insert(object)
        .run(conn)
        .catch(err => {
            return Promise.reject(err)
        });

};

const updateUser = (user, conn) => {

    return r.db('invirohub_mobile').table('users').get(user.id)
        .update(user ,
            {returnChanges: true})
        .run(conn)
        .catch(err => {
            return Promise.reject(err);
        })

};

const runUpdates = (event, conn) => {

    return Promise.all([transactionInsert(event.transactions[0], conn), updateUser(event.user, conn)])
        .then(r => {
            console.log('updates: ', r);
            const result = r[1].changes[0].new_val;
            if(result) {
                return event;
            } else {
                return Promise.reject({msg: `Error saving user to database`})
            }

        })

};

const fetchToken = event => {

    const id = event.input.tokenId;

    const restPath = `http://vend-env.us-east-1.elasticbeanstalk.com/api/vend`;

    const meter = event.user.meters.filter(m => m.id === event.input.meterId).map(me => me.number)[0];

    const payload = Object.assign({}
        , { "request_id": id }
        , { "request_type": "CP" }
        , { "payload": {
            "meter_number": meter
            , "amount":  1 // event.input.amount
            , "tendered": 1 //event.input.amount
            , "api": 5
        }
        });

    return new Promise( (resolve, reject) => {

        request.post(
            restPath
            , { json: payload }
            , function (error, response, body) {
                if (!error && response.statusCode == 200) {
                    let result = Object.assign(event, { cPResult: body });
                    console.log('fetchToken res: ', result);
                    resolve(result);
                } else {
                    reject({msg: 'error with http'});
                }
            })

    });


};

const createTransaction = (event) => {
    console.log('CPResult: ', JSON.stringify(event.cPResult));

    const credit = 0;
    const debit = event.input.amount;
    const id = event.input.tokenId;

    const stsToken = event.cPResult.payload.sts_token;
    const kWh = event.cPResult.payload.total_units_issued;
    const cPReceipt = event.cPResult.payload.receipt_num;

    const meterNo = event.user.meters.filter(m => m.id === event.input.meterId).map(me => me.number)[0];

    const values = [
        event.input.amount
        , 0.00
        , 0.00
        , 0.00
        , 0.00
        , 0.00
        , 0.00
        , 0.00
        , event.cPResult.payload.amount_tendered
        , event.cPResult.payload.vat_amount
        , event.cPResult.payload.value_ex_vat
    ];

    const keys = ['totalAmount', 'creditCharge', 'creditChargeVat', 'tokenCharge', 'tokenChargeVat',
        'resellerCommission', 'inviroCommission', 'commissionVat', 'energy', 'energyVat', 'vendAmount'];

    const calc = R.zipObj(keys, values);

    const transaction = Object.assign({}
        , { id: id }
        , { userId: event.user.id }
        , { date: new Date() }
        , { paymentMethod: event.input.paymentMethod }
        , { transactionType: 'tokenPurchase'}
        , { debit: debit }
        , { credit: credit }
        , { paygateReferenceNumber: event.input.paygateMerchantOrderId }
        , { paygateRequestId: event.input.paygateRequestId }
        , { cPReceiptNumber: cPReceipt }
        , { meterId: event.input.meterId }
        , { meterNumber: meterNo }
        , { meterType: event.input.meterType }
        , { stsToken: stsToken }
        , { kWh: kWh }
        , { costs: calc }
        , { paygateTransactionStatus: '1' }
        , { paygateTransactionDescription: 'Approved'}
    );
    const userTransactionLog = Object.assign({}
        , { id: id }
        , { date: new Date() }
        , { meter: meterNo }
        , { stsToken: stsToken }
        , { debit: debit }
        , { credit: credit }
        , { kWh: kWh }
        , { paymentMethod: event.input.paymentMethod }
        , { paygateReferenceNumber: event.input.paygateMerchantOrderId }
        , { cPReceiptNumber: cPReceipt }
    );

    const userNotification = Object.assign({}
        , { id: uuid() }
        , { title: "Token Purchase" }
        , { date: new Date() }
        , { message: `You have purchased ${kWh} kWh for R ${event.input.amount}.00 for meter: ${meterNo}. Your STS Token number is ${stsToken}.`  }
        , { stsToken: stsToken }
        , { isRead: false }
    );

    console.log('Voltex transaction log: ', transaction, 'User transaction Log: ', userTransactionLog);

    event.user.transactionLogs.unshift(userTransactionLog);
    event.transactions.unshift(transaction);
    event.user.notifications.unshift(userNotification);

    return Promise.resolve(event);
};

const cityPowerToken = (event, conn) => {
    return fetchToken(event)
        .then(event => createTransaction(event))
        .then(event => runUpdates(event, conn))
};

const tokenPurchase = input => {
    return r.connect(config).then(conn => {
        return getUser(input, conn)
            .then(event => cityPowerToken(event, conn))
            .then(result => { conn.close(); return result; })
            .catch(err => { conn.close(); return Promise.reject(err); });
    });
};

module.exports = { tokenPurchase: tokenPurchase };
